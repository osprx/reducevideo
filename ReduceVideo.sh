#!/bin/bash

#---------------------------------------------------------------
#   Project name			ReduceVideo
#	Version					1.0 
#	Main File				ReduceVideo.sh
#	Project start date		2019-04-30	 
#	Created By				Janos Wunderlich
#	
# ReduceVideo
#
# **Description**:
#  
# Linux shell script to **reduce** the **video** size in the entire directory with the **ffmpeg** tool, copying the new generated files with its own name plus "r" (for reduced).
# This shell script is a good solution to reduce the size of the video in a quick way, e.g. if you want to send it with email or messenger.  
#   
# **instructions**:  
#   
# Copy the shell script to the ~/bin directory as root and make it executable.
#   
# **Limitations**:  
#   
# - The shell script renames files in the entire directory only.
#   
# **Known issues**:  
#   
# - Files without extension are not correctly processed
#
#---------------------------------------------------------------



#---------------------------------------------------------------
#   ReduceVideo.sh main script
#	reduce video files
#---------------------------------------------------------------

# For Loop File Names With Spaces:
# http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
# without this are filenames with spaces not correctly processed.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

# for security reasons, define accepted file extensions here.
# .tsf and .tsx extensions are for testing purposes only
i=0
echo "check for file types..."
# note: 2>/dev/null redirects error messages to /dev/null
files=$( find *.{mp4,mpeg4,avi,divx,mov,MP4,MPEG4,AVI,DIVX,MOV,tsf,tsx} 2>/dev/null ) 
#echo $files

# handle all determined files
for file in $files; do
    #Help:
    #https://tecadmin.net/how-to-extract-filename-extension-in-shell-script/
    
    #Get Filename without Path:
	fullfilename=$file
	filename=$(basename "$fullfilename")
	
	#extract Filename without Extension
	fname="${filename%.*}"
	#echo $fname
	
	#extract Extension from Filename
	EXTENSION="${filename##*.}"
	
	#build new filename for reduced video file
	REDUCED_FILENAME=$fname'r'.$EXTENSION
		

	echo reduce $file to half: $REDUCED_FILENAME	
	ffmpeg -i "$file" -vf "scale=iw/2:ih/2" $REDUCED_FILENAME
	#	echo move $file to $TIMESTAMP_FILENAME		
	#	mv $file $TIMESTAMP_FILENAME

	sleep 1
((i++))
done	# for file in *

IFS=$SAVEIFS

echo "$i files are processed."


